/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3_xo;

/**
 *
 * @author 11pro
 */
class OXProgram {

    static boolean checkWin(char[][] table, char currentPlayer) {
        if (checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkX1(table, currentPlayer) || checkX2(table, currentPlayer)) {
            return true;
        }

        return false;
    }

    private static boolean checkRow(char [][] table, char currentPlayer) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }
    private static boolean checkCol(char [][] table, char currentPlayer) {
        for (int col = 0; col<3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkRow(char [][] table, char currentPlayer, int row) {
       for (int i = 0; i < 3; i++) {
            if (table[row][i]!= currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] table, char currentPlayer, int col) {
       for (int j = 0; j < 3; j++) {
            if (table[j][col]!= currentPlayer) {
                return false;
            }
        }
        return true;
    }
    private static boolean checkX1(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
        private static boolean checkX2(char[][] table, char currentPlayer) {
       for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDraw(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
